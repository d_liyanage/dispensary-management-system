package com.suppliermanagement.controllers.Stock_Control_CTRL;

import com.common.ControlledScreen;
import com.common.ScreenController;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Naveen Luke Fernando on 2017-09-20.
 */
public class Stock_Control_New_Purchase_CTRL implements ControlledScreen, Initializable {
    @Override
    public void setScreenParent(ScreenController screenParent) {

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}
